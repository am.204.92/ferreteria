
(function () {
  'use strict';

  angular.module('BlurAdmin.api', ['restangular'])
    .config(apiConfig);

  apiConfig.$inject = ['RestangularProvider'];
  function apiConfig(RestangularProvider) {
    RestangularProvider.setBaseUrl('http://localhost:8000/api');

    if (window.location.host.indexOf('localhost') < 0) {
      RestangularProvider.setBaseUrl(window.location.origin + '/api');
    }
  }

})();
